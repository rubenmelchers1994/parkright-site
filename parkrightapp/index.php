<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ParkRight!</title>
	<link rel="stylesheet" href="css/style.css">
</head>
<body>

	<section class="intro">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h1 class="intro__title">ParkRight!</h1>
				</div>

				<div class="col-12 intro__description">
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam iure nisi repellendus sunt quidem nihil neque voluptates consequuntur assumenda dolorem cupiditate atque iste sed delectus tempore repellat nemo, mollitia expedita quibusdam vero quam impedit voluptatem possimus distinctio inventore. Adipisci unde neque qui praesentium porro animi vitae, quasi aliquam quae sequi!
					</p>
				</div>

			</div>
		</div>
	</section>

	<section class="apidata" id="apidata">
		<div class="container">
			<div class="row apidata__wrapper"></div>
		</div>
	</section>

	<?php

	include "footer.php";
	?>