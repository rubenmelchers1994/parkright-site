<?php
/**
 * The template for displaying the footer
**/
?>
<footer class="footer">
	<div class="wrap">
		<div class="row">
			<div class="col-lg-12">
				<p class="footer__copyright">
					©Ruben Melchers, 2017
				</p>
			</div>
		</div>
	</div>
</footer>
<!--build:js js/main.min.js -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="js/main.min.js" type="text/javascript"></script>
<!-- endbuild -->
</body>
</html>

