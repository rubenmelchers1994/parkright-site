(function() {
	'use strict';

	var API_URL = "https://stud.hosted.hr.nl/0889496/Jaar%203/parkright/",
		LICENCE_API_URL = "https://api.openalpr.com/v2/recognize?secret_key=sk_0bdb4200cfc3cc69b0a85458&recognize_vehicle=0&country=eu&state=nl&return_image=0&topn=10",
		RDW_API_URL = "https://opendata.rdw.nl/resource/m9d7-ebf2.json?kenteken=",
		container = document.querySelector('.apidata');

	if(!container) {
		return;
	}

	var datawrapper = container.querySelector('.apidata__wrapper');

	function loadJSON(path, success, error)	{
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function()
		{
			if (xhr.readyState === XMLHttpRequest.DONE) {
				if (xhr.status === 200) {
					if (success)
						success(JSON.parse(xhr.responseText));
				} else {
					if (error)
						error(xhr);
				}
			}
		};
		xhr.open("GET", path, true);
		xhr.overrideMimeType("application/json"),
		xhr.setRequestHeader("accept", "application/json");
		xhr.send();
	}

	loadJSON(API_URL, function(data) {

		handleData(data);

	}, function(xhr) {
		console.error(xhr);
	});


	function handleData(data) {
		for (var i = data.items.length - 1; i >= 0; i--) {

			var lastEntry = i - 1;


			var thisData = data.items[i],
				id = data.items[i].id,
				title = data.items[i].title,
				photo = data.items[i].photo,
				description = data.items[i].description,
				location = data.items[i].location,
				licence = data.items[i].licence;

			if(!datawrapper) {
				return;
			}

			var	item = document.createElement("div"),
				row = document.createElement("div"),
				titleColumn = document.createElement("div"),
				leftColumn = document.createElement("div"),
				rightColumn = document.createElement("div"),
				idElement = createElement("id", id),
				titleElement = createElement("title", title),
				photoElement = createElement("photo", photo),
				descriptionElement = createElement("description", description),
				locationElement = createElement("location", location),
				deleteElement = createDeleteElement("delete", id),
				licenceElement;


			titleColumn.appendChild(idElement);

			leftColumn.appendChild(photoElement);
			leftColumn.appendChild(locationElement);

			rightColumn.appendChild(titleElement);
			rightColumn.appendChild(descriptionElement);


			if (licence == null || licence == "undefined" || !licence || licence >= 0 || licence == "") {

				//get licence plate from picture
				licence = handleLicence(photoElement, photo, rightColumn);

			} else {

				licenceElement = createElement("licence", licence);
				rightColumn.appendChild(licenceElement);
				getVehicleInfo(licence, rightColumn);

			}




			item.classList.add("col-12");
			item.classList.add("apidata__item");
			row.classList.add("row");
			titleColumn.classList.add("col-12");
			leftColumn.classList.add("col-6", "col-sm-12", "col-xs-12");
			rightColumn.classList.add("col-6", "col-sm-12", "col-xs-12");

			item.appendChild(deleteElement);
			row.appendChild(titleColumn);
			row.appendChild(leftColumn);
			row.appendChild(rightColumn);
			item.appendChild(row);

			datawrapper.appendChild(item);
		}
	}

	function handleLicence(element, data, parentElement) {

		var hasImage = element.getElementsByTagName("img");

		if ( (hasImage == null) || (hasImage.length == 0) ) {
			return;
		}

		var imageSrc = element.getElementsByTagName("img")[0].src,
			formData = new FormData(),
			xhr = new XMLHttpRequest(),
			contentType = "image/jpeg",
			blob = b64toBlob(data, contentType),
			blobUrl = URL.createObjectURL(blob),
			licenceToShow = "";

		formData.append("image", blob);


		/* ---------------------- UNCOMMENT BELOW FOR ACTUAL DATA ---------------------- */

		xhr.open('POST', LICENCE_API_URL);

		xhr.send(formData);

		xhr.onreadystatechange = function() {
			if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
				var responseJson = xhr.responseText;
				var receivedLicencePlate = "";

				responseJson = JSON.parse(responseJson);

				if(!responseJson || responseJson == null || responseJson == "undefined" || responseJson == undefined) {
					receivedLicencePlate = "Geen kenteken gevonden";

					licenceToShow = receivedLicencePlate;

					var licenceElement = createElement("licence", licenceToShow);
					parentElement.appendChild(licenceElement);


				} else {


					if ( typeof responseJson.results[0] == "undefined" || typeof responseJson.results[0].plate == "undefined" || typeof responseJson.results[0].plate == null || typeof responseJson.results[0].plate >= 0 || typeof responseJson.results[0].plate == undefined) {
						receivedLicencePlate = "Geen kenteken gevonden";

						licenceToShow = receivedLicencePlate;


						var licenceElement = createElement("licence", licenceToShow);
						parentElement.appendChild(licenceElement);


					} else {
						receivedLicencePlate = responseJson.results[0].plate;
						licenceToShow = receivedLicencePlate;

						var licenceElement = createElement("licence", licenceToShow);
						parentElement.appendChild(licenceElement);

						getVehicleInfo(licenceToShow, parentElement);
					}
				}
			}
			licenceToShow =  receivedLicencePlate;

		}

		/* ---------------------- UNCOMMENT ABOVE FOR ACTUAL DATA ---------------------- */

	}

	function getVehicleInfo(licence, parent) {
		var licenceToCheck = licence.toUpperCase(),
			urlToCheck = RDW_API_URL + licenceToCheck;

		loadJSON(urlToCheck, function(data) {

			//car info to variables
			var vehicleObject = data[0],
				creationDate = vehicleObject.datum_eerste_toelating,
				brand = vehicleObject.merk,
				vehicleKind = vehicleObject.voertuigsoort,
				insured = vehicleObject.wam_verzekerd,
				apkDate = vehicleObject.vervaldatum_apk,
				tradeName = vehicleObject.handelsbenaming;

			var parentElement = parent;

			var creationDateEl = createElement("gebouwd-op", creationDate),
				brandEl = createElement("merk", brand),
				kindEl = createElement("voertuigsoort", vehicleKind),
				insuranceEl = createElement("WA-verzekerd", insured),
				apkEl = createElement("apk-vervaldatum", apkDate),
				tradeEl = createElement("handelsnaam", tradeName);

			parent.appendChild(creationDateEl);
			parent.appendChild(brandEl);
			parent.appendChild(kindEl);
			parent.appendChild(insuranceEl);
			parent.appendChild(apkEl);
			parent.appendChild(tradeEl);

		}, function(xhr) {
			console.error(xhr);
		});
	}




	function b64toBlob(b64Data, contentType, sliceSize) {
		contentType = contentType || '';
		sliceSize = sliceSize || 512;

		var byteCharacters = atob(b64Data);
		var byteArrays = [];

		for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
			var slice = byteCharacters.slice(offset, offset + sliceSize);

			var byteNumbers = new Array(slice.length);
			for (var i = 0; i < slice.length; i++) {
				byteNumbers[i] = slice.charCodeAt(i);
			}

			var byteArray = new Uint8Array(byteNumbers);

			byteArrays.push(byteArray);
		}

		var blob = new Blob(byteArrays, {type: contentType});
		return blob;
	}






	function createElement(property, data) {

		if(property == "photo") {
			var base64Matcher = new RegExp("^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})$");

			var	element = document.createElement("div");

			if(base64Matcher.test(data)) {

				var baseUrl = "data:image/jpeg;base64,",
				image = new Image(),
				imageSource = baseUrl += data;

				image.src = imageSource;
				element.appendChild(image);

			} else {
				var innerElement = document.createTextNode("" + property + ": there is no photo");

				element.appendChild(innerElement);

			}

		} else if (property == "location") {

			var mapsUrl = "http://maps.google.com/?q=" + data,
			element = document.createElement("a"),
			innerText = document.createTextNode("Bekijk locatie van gemaakte foto");

			element.setAttribute("href", mapsUrl),
			element.setAttribute("target", "_blank"),
			element.appendChild(innerText);

		} else if (property == "delete") {

			var element = document.createElement("div"),
				tooltip = document.createElement("div"),
				tooltipText = document.createTextNode("Verwijder entry");

			tooltip.classList.add("apidata__tooltip");
			tooltip.appendChild(tooltipText);

			element.addEventListener("click", function() {
				console.log("clicked " + element);
				console.log(element);

			});

			element.appendChild(tooltip);


		} else {
			var element = document.createElement("div"),
			innerElement = document.createTextNode("" + property + ": " + data);

			element.appendChild(innerElement);
		}

		element.classList.add("apidata__" + property);

		return element;
	}

	function createDeleteElement(property, id) {
		var element = document.createElement("div"),
			tooltip = document.createElement("div"),
			tooltipText = document.createTextNode("Verwijder entry");

		tooltip.classList.add("apidata__tooltip");
		tooltip.appendChild(tooltipText);

		element.addEventListener("click", function() {


			deleteEntry(id);

		});

		element.appendChild(tooltip);
		element.classList.add("apidata__" + property);

		return element;
	}

	function deleteEntry(id) {
		var idFromEntry = id;

		console.log("clicked delete button on entry " + id);

		var url = "https://stud.hosted.hr.nl/0889496/Jaar%203/parkright/?id=" + id,
			xhr = new XMLHttpRequest();

		xhr.open("DELETE", url, true);

		xhr.onload = function () {
			if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == "204") {
				location.reload();
			} else {
				console.log("error");
			}
		}
		xhr.send(null);

	}



})();